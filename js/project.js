var serverRequest = {};
serverRequest.getOKPD2 = function() {
	var okpd2 =
		{
			"OKPD2": [
				{
					"OKPD2Code": "32.10.62.119",
					"OKPD2Text": "Схемы интегральные МОП-структурные цифровые монолитные прочие"
				},
				{
					"OKPD2Code": "51.86.10.110",
					"OKPD2Text": "Услуги по оптовой торговле электронными приборами и прочими радиокомпонентами"
				},
				{
					"OKPD2Code": "32.10.62.311",
					"OKPD2Text": "Схемы интегральные аналоговые и цифро-аналоговые монолитные: усилители"
				},
				{
					"OKPD2Code": "72.50.11",
					"OKPD2Text": "Услуги по техническому обслуживанию и ремонту офисных машин"
				}
			]
		};
	return JSON.stringify(okpd2);
}
serverRequest.getCompanies = function() {
	var companies =
		{
			"time": "2019-07-08 12:30:46",
			"params": {
				"minRate": ".0",
				"applyMode": "agsFind",
				"topNumber": "100"
			},
			"actorId": null,
			"taskHId": "6842edf0-7884-441f-8169-4aacec9a5d12",
			"agsActors": {
				"actors": [{
					"rate": 0.3,
					"actorId": 200071096,
					"bgColor": "hsla(69.80, 53.95%, 48.82%, 1.00)",
					"actorHId": "5a70a153-bf63-7f18-e6db-d0d962f82076",
					"actorINN": [
						"3662209722"
					],
					"actorKey": "18070274",
					"actorCats": null,
					"actorName": "ООО \"НПП \"МИКРОЭЛЕКТРОНИКА\"",
					"actorOGRN": "1143668053033",
					"rateOrder": 1
				},
					{
						"rate": 0.2,
						"actorId": 200174880,
						"bgColor": "hsla(69.80, 53.95%, 48.82%, 1.00)",
						"actorHId": "72d3d659-1e30-973a-d0cd-7481b4943ad6",
						"actorINN": [
							"7710620481"
						],
						"actorKey": "15121978",
						"actorCats": null,
						"actorName": "ООО \"ТЕКСИС ГРУП\"",
						"actorOGRN": "1067746362750",
						"rateOrder": 2
					},
					{
						"rate": 0.4,
						"actorId": 200114502,
						"bgColor": "hsla(69.80, 53.95%, 48.82%, 1.00)",
						"actorHId": "5c139fa3-50e2-cc9b-768d-3e521a1c1f86",
						"actorINN": [
							"7810540857"
						],
						"actorKey": "19778947",
						"actorCats": null,
						"actorName": "ООО \"ЭЛТЕХ КОМПОНЕНТ\"",
						"actorOGRN": "1087847024363",
						"rateOrder": 3
					},
					{
						"rate": 0.89,
						"actorId": 200067152,
						"bgColor": "hsla(69.79, 53.95%, 48.82%, 1.00)",
						"actorHId": "250e4cfd-b3ad-6079-bdde-792fea44cbbf",
						"actorINN": [
							"5402563806"
						],
						"actorKey": "19039724",
						"actorCats": null,
						"actorName": "ООО \"Б4ЭКСПРЕСС\"",
						"actorOGRN": "1135476091211",
						"rateOrder": 4
					},
					{
						"rate": 0.6,
						"actorId": 200157637,
						"bgColor": "hsla(69.79, 53.95%, 48.82%, 1.00)",
						"actorHId": "e3e737f1-1a13-f40f-c447-c156471b6eb0",
						"actorINN": [
							"7811530756"
						],
						"actorKey": "19820287",
						"actorCats": null,
						"actorName": "ООО \"НЬЮТЭК\"",
						"actorOGRN": "1127847473753",
						"rateOrder": 5
					},
					{
						"rate": 0.89,
						"actorId": 200087585,
						"bgColor": "hsla(69.79, 53.95%, 48.82%, 1.00)",
						"actorHId": "e5fb1658-0e87-017a-da0a-76f5fb7759de",
						"actorINN": [
							"7735521475"
						],
						"actorKey": "1629690",
						"actorCats": null,
						"actorName": "АО \"НИИТАП\"",
						"actorOGRN": "1067746781024",
						"rateOrder": 6
					},
					{
						"rate": 0.89,
						"actorId": 200174671,
						"bgColor": "hsla(69.79, 53.95%, 48.82%, 1.00)",
						"actorHId": "eb319ec2-11ef-b0f0-4883-4b05a420dc34",
						"actorINN": [
							"7735597072"
						],
						"actorKey": "15361655",
						"actorCats": null,
						"actorName": "ООО \"ИПК \"ЭЛЕКТРОН-МАШ\"",
						"actorOGRN": "1137746960911",
						"rateOrder": 7
					},
					{
						"rate": 0.89,
						"actorId": 200070895,
						"bgColor": "hsla(69.79, 53.95%, 48.82%, 1.00)",
						"actorHId": "70fffa0e-e261-cb6c-1efa-1ba9b7f19af6",
						"actorINN": [
							"7709447183"
						],
						"actorKey": "15611912",
						"actorCats": null,
						"actorName": "ООО \"ОНЭЛЕК\"",
						"actorOGRN": "1157746034820",
						"rateOrder": 8
					},
					{
						"rate": 0.89,
						"actorId": 200084080,
						"bgColor": "hsla(69.79, 53.95%, 48.82%, 1.00)",
						"actorHId": "a9965952-096b-9966-160b-4b3ced887cac",
						"actorINN": [
							"7722846740"
						],
						"actorKey": "15572422",
						"actorCats": null,
						"actorName": "ООО \"РЕ-ИНЖИНИРИНГ\"",
						"actorOGRN": "1147746686559",
						"rateOrder": 9
					},
					{
						"rate": 0.89,
						"actorId": 200147866,
						"bgColor": "hsla(69.79, 53.95%, 48.82%, 1.00)",
						"actorHId": "88fd0486-4b7f-24a7-8472-13b9bb3d80b7",
						"actorINN": [
							"7802197604"
						],
						"actorKey": "19687729",
						"actorCats": null,
						"actorName": "ООО \"СТАНДАРТ ВПК\"",
						"actorOGRN": "1027801527533",
						"rateOrder": 10
					},
					{
						"rate": 0.89,
						"actorId": 200043949,
						"bgColor": "hsla(69.78, 53.96%, 48.83%, 1.00)",
						"actorHId": "28015a83-0f2a-6670-fd19-ccc14974638c",
						"actorINN": [
							"7731631438"
						],
						"actorKey": "15105798",
						"actorCats": null,
						"actorName": "АО \"РАДИОПРИБОРСНАБ\"",
						"actorOGRN": "1097746424181",
						"rateOrder": 11
					},
					{
						"rate": 0.89,
						"actorId": 200080634,
						"bgColor": "hsla(69.78, 53.96%, 48.83%, 1.00)",
						"actorHId": "1789f20b-84df-a025-675d-f4b01e58dfb5",
						"actorINN": [
							"7725844163"
						],
						"actorKey": "1863198",
						"actorCats": null,
						"actorName": "ООО \"КОМПАНИЯ МИКРОМАК\"",
						"actorOGRN": "5147746175154",
						"rateOrder": 12
					},
					{
						"rate": 0.89,
						"actorId": 200023136,
						"bgColor": "hsla(69.78, 53.96%, 48.84%, 1.00)",
						"actorHId": "bfc79663-0a87-aaa5-ca67-41080d324177",
						"actorINN": [
							"7451355908"
						],
						"actorKey": "18684782",
						"actorCats": null,
						"actorName": "ООО \"МИКРОСХЕМА\"",
						"actorOGRN": "1137451011114",
						"rateOrder": 13
					},
					{
						"rate": 0.89,
						"actorId": 200055092,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "27157af4-7859-1bce-9907-6f22bdf648ef",
						"actorINN": [
							"4026000108"
						],
						"actorKey": "20467005",
						"actorCats": [
							5
						],
						"actorName": "АО \"ВОСХОД\"-КРЛЗ",
						"actorOGRN": "1024001425910",
						"rateOrder": 14
					},
					{
						"rate": 0.89,
						"actorId": 200166620,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "ca7ba81d-21a7-ce34-9006-77e591139b5c",
						"actorINN": [
							"7743937237"
						],
						"actorKey": "15584224",
						"actorCats": null,
						"actorName": "НАО \"НПЦ \"СПС\"",
						"actorOGRN": "1147746952165",
						"rateOrder": 15
					},
					{
						"rate": 0.89,
						"actorId": 200106759,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "7e31a035-af27-139d-e55e-a62142edee36",
						"actorINN": [
							"7721603167"
						],
						"actorKey": "15507009",
						"actorCats": null,
						"actorName": "ООО \"А-СТ\"",
						"actorOGRN": "1077762544221",
						"rateOrder": 16
					},
					{
						"rate": 0.89,
						"actorId": 200169837,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "78c7fbb9-06b6-1f5a-a4ea-87d0bea86398",
						"actorINN": [
							"7728275701"
						],
						"actorKey": "15059806",
						"actorCats": null,
						"actorName": "ООО \"АСТ КОМПОНЕНТС\"",
						"actorOGRN": "1037728000243",
						"rateOrder": 17
					},
					{
						"rate": 0.89,
						"actorId": 200059046,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "5d4f74a1-093c-c4e4-27d8-835b99a29a52",
						"actorINN": [
							"7735094030"
						],
						"actorKey": "15470368",
						"actorCats": null,
						"actorName": "ООО \"ИТЦ МП\"",
						"actorOGRN": "1047735008419",
						"rateOrder": 18
					},
					{
						"rate": 0.89,
						"actorId": 200015552,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "146417dd-3a10-f6a7-ec82-b3da9059e1fb",
						"actorINN": [
							"7805307157"
						],
						"actorKey": "19621271",
						"actorCats": null,
						"actorName": "ООО \"КОНТАКТ ЭЛЕКТРОНИКС\"",
						"actorOGRN": "1157847062966",
						"rateOrder": 19
					},
					{
						"rate": 0.89,
						"actorId": 200132954,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "fd2de005-6c89-95a7-f8e1-22eb9e9d46ef",
						"actorINN": [
							"9705123400"
						],
						"actorKey": "24810389",
						"actorCats": null,
						"actorName": "ООО \"МЕРКУРИЙ\"",
						"actorOGRN": "1187746818962",
						"rateOrder": 20
					},
					{
						"rate": 0.89,
						"actorId": 200008903,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "f107aeca-d14f-b05d-6350-8f1f34e494aa",
						"actorINN": [
							"7811603796"
						],
						"actorKey": "19818246",
						"actorCats": null,
						"actorName": "ООО \"НПО \"КАСКАД\"",
						"actorOGRN": "1167847141417",
						"rateOrder": 21
					},
					{
						"rate": 0.89,
						"actorId": 200102911,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "99a17e5a-b05f-35e3-c405-f3bd244b8381",
						"actorINN": [
							"7735565264"
						],
						"actorKey": "15811782",
						"actorCats": null,
						"actorName": "ООО \"ПРАЙМ\"",
						"actorOGRN": "1097746825945",
						"rateOrder": 22
					},
					{
						"rate": 0.89,
						"actorId": 200047641,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "1db48b60-156b-88cc-00f4-94cc316eb403",

						"actorINN": [
							"5904286761"
						],
						"actorKey": "18330095",
						"actorCats": null,
						"actorName": "ООО \"РК\"",
						"actorOGRN": "1135904005962",
						"rateOrder": 23
					},
					{
						"rate": 0.89,
						"actorId": 200151109,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "1e89a4fa-9c3c-6a95-6248-364ced70af98",
						"actorINN": [
							"7701706284"
						],
						"actorKey": "1804934",
						"actorCats": null,
						"actorName": "ООО \"РУСТЕХТРЕЙД\"",
						"actorOGRN": "1077746369051",
						"rateOrder": 24
					},
					{
						"rate": 0.89,
						"actorId": 200103336,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "db39a495-bfe4-c908-9600-bd798db648e3",
						"actorINN": [
							"7707366122"
						],
						"actorKey": "15758522",
						"actorCats": null,
						"actorName": "ООО \"ТРЕЙД-КОМПОНЕНТ\"",
						"actorOGRN": "1167746452532",
						"rateOrder": 25
					},
					{
						"rate": 0.89,
						"actorId": 200009389,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "d107465a-a18c-40ca-946b-98261cc2428a",
						"actorINN": [
							"7805584256"
						],
						"actorKey": "19810992",
						"actorCats": null,
						"actorName": "ООО \"ЧИП ЛОГИК ПЛЮС\"",
						"actorOGRN": "1127847172155",
						"rateOrder": 26
					},
					{
						"rate": 0.89,
						"actorId": 200138317,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "434a1d10-44fe-4ea7-6815-a06d44e6c917",
						"actorINN": [
							"7816254997"
						],
						"actorKey": "19618896",
						"actorCats": null,
						"actorName": "ООО \"ЭЛКО СЕВЕРО-ЗАПАД\"",
						"actorOGRN": "1157847104436",
						"rateOrder": 27
					},
					{
						"rate": 0.89,
						"actorId": 200146326,
						"bgColor": "hsla(69.77, 53.97%, 48.84%, 1.00)",
						"actorHId": "a6f9cf69-efb2-910b-45f4-586992f91377",
						"actorINN": [
							"7810307681"
						],
						"actorKey": "19730533",
						"actorCats": null,
						"actorName": "ООО \"ЭЛМИКОМ-ТВС\"",
						"actorOGRN": "1037821122790",
						"rateOrder": 28
					},
					{
						"rate": 0.89,
						"actorId": 200166720,
						"bgColor": "hsla(69.77, 53.97%, 48.85%, 1.00)",
						"actorHId": "50411cf5-de30-4a46-1cb2-c42aebd1fc9f",
						"actorINN": [
							"5402016507"
						],
						"actorKey": "18976482",
						"actorCats": null,
						"actorName": "ООО \"АЙСИ ИМПОРТ\"",
						"actorOGRN": "1165476077095",
						"rateOrder": 29
					},
					{
						"rate": 0.89,
						"actorId": 200125619,
						"bgColor": "hsla(69.75, 53.98%, 48.86%, 1.00)",
						"actorHId": "22a08578-56de-8d8e-039e-ac66cfd81795",
						"actorINN": [
							"7719555580"
						],
						"actorKey": "15105289",
						"actorCats": null,
						"actorName": "ЗАО \"СФЕРА\"",
						"actorOGRN": "1057747128427",
						"rateOrder": 30
					},
					{
						"rate": 0.89,
						"actorId": 200175919,
						"bgColor": "hsla(69.75, 53.98%, 48.86%, 1.00)",
						"actorHId": "3c4cbe77-ce6c-ed5b-1308-7627f0799680",
						"actorINN": [
							"7724857635"
						],
						"actorKey": "15317626",
						"actorCats": null,
						"actorName": "АО \"СОЗВЕЗДИЕ\"",
						"actorOGRN": "1127747248892",
						"rateOrder": 31
					},
					{
						"rate": 0.89,
						"actorId": 200116046,
						"bgColor": "hsla(69.75, 53.98%, 48.86%, 1.00)",
						"actorHId": "d114cfa7-6286-9080-9e61-fda31450afea",
						"actorINN": [
							"7714346649"
						],
						"actorKey": "15655838",
						"actorCats": null,
						"actorName": "ООО \"ИНОВАСИСТЕМ\"",
						"actorOGRN": "1157746622000",
						"rateOrder": 32
					},
					{
						"rate": 0.89,
						"actorId": 200050470,
						"bgColor": "hsla(69.75, 53.98%, 48.86%, 1.00)",
						"actorHId": "4a64a298-670d-b09f-8963-ee77b0cdc1a3",
						"actorINN": [
							"7735164262"
						],
						"actorKey": "15928162",
						"actorCats": null,
						"actorName": "ООО \"МИРКОМПОНЕНТОВ\"",
						"actorOGRN": "1177746671365",
						"rateOrder": 33
					},
					{
						"rate": 0.89,
						"actorId": 200090611,
						"bgColor": "hsla(69.75, 53.98%, 48.86%, 1.00)",
						"actorHId": "15c9bc3b-cdff-37eb-6df0-82ff47d519b6",
						"actorINN": [
							"5029166199"
						],
						"actorKey": "15304390",
						"actorCats": null,
						"actorName": "ООО \"РАДИОТЕСТКОМПЛЕКТ\"",
						"actorOGRN": "1125029008280",
						"rateOrder": 34
					},
					{
						"rate": 0.89,
						"actorId": 200031402,
						"bgColor": "hsla(69.75, 53.98%, 48.86%, 1.00)",
						"actorHId": "1cef52b7-2c23-1198-9cd7-2ff7b3af7cf2",
						"actorINN": [
							"3662135414"
						],
						"actorKey": "18037282",
						"actorCats": null,
						"actorName": "ООО \"СИГМА-ПРОЕКТ\"",
						"actorOGRN": "1083668025385",
						"rateOrder": 35
					},
					{
						"rate": 0.89,
						"actorId": 200016107,
						"bgColor": "hsla(69.74, 53.99%, 48.87%, 1.00)",
						"actorHId": "e6acc6b7-d436-48ef-d734-bb356abc0edb",
						"actorINN": [
							"7736261847"
						],
						"actorKey": "15738109",
						"actorCats": null,
						"actorName": "ООО \"ДЭРИ ГРУПП\"",
						"actorOGRN": "1167746190182",
						"rateOrder": 36
					},
					{
						"rate": 0.89,
						"actorId": 200021286,
						"bgColor": "hsla(69.74, 53.99%, 48.87%, 1.00)",
						"actorHId": "fd04db94-5da9-8de7-d695-5cc3260c2600",
						"actorINN": [
							"6319183230"
						],
						"actorKey": "18829465",
						"actorCats": null,
						"actorName": "ООО ТД \"РАДИОСВЕТ\"",
						"actorOGRN": "1146319006679",
						"rateOrder": 37
					},
					{
						"rate": 0.889,
						"actorId": 200006793,
						"bgColor": "hsla(69.71, 54.01%, 48.90%, 1.00)",
						"actorHId": "9d939bfb-25ec-116f-dd98-9c6d2fcbd874",
						"actorINN": [
							"5254082662"
						],
						"actorKey": "18592427",
						"actorCats": null,
						"actorName": "ООО \"НПП ИНТЕХ\"",
						"actorOGRN": "1085254000490",
						"rateOrder": 38
					},
					{
						"rate": 0.889,
						"actorId": 200051981,
						"bgColor": "hsla(69.70, 54.01%, 48.91%, 1.00)",
						"actorHId": "1d288a9e-a0c2-0c1d-ce1a-d8f4e8d4b99b",
						"actorINN": [
							"6143064243"
						],
						"actorKey": "18461215",
						"actorCats": null,
						"actorName": "ООО \"РУССКИЙ СТИЛЬ СТРОЙ\"",
						"actorOGRN": "1066143052414",
						"rateOrder": 39
					},
					{
						"rate": 0.889,
						"actorId": 200038718,
						"bgColor": "hsla(69.70, 54.02%, 48.91%, 1.00)",
						"actorHId": "8eaeb6d3-3058-20e3-0ed9-64b81082f004",
						"actorINN": [
							"7733116954"
						],
						"actorKey": "1638316",
						"actorCats": null,
						"actorName": "ООО \"ТПС\"",
						"actorOGRN": "1037733045019",
						"rateOrder": 40
					},
					{
						"rate": 0.889,
						"actorId": 200020956,
						"bgColor": "hsla(69.67, 54.04%, 48.94%, 1.00)",
						"actorHId": "46321849-2038-1156-7ea3-9cc740ff2613",
						"actorINN": [
							"5404255050"
						],
						"actorKey": "19054576",
						"actorCats": null,
						"actorName": "ООО \"КОМПАНИЯ \"ТРАЙСЕЛЬ\"",
						"actorOGRN": "1055404114590",
						"rateOrder": 41
					},
					{
						"rate": 0.888,
						"actorId": 200035149,
						"bgColor": "hsla(69.57, 54.10%, 49.03%, 1.00)",
						"actorHId": "0a3c1aab-d461-2bf3-de6c-99109b2efc9c",
						"actorINN": [
							"7719582826"
						],
						"actorKey": "15121904",
						"actorCats": null,
						"actorName": "АО \"РАДИАНТ-ЭЛКОМ\"",
						"actorOGRN": "1067746336085",
						"rateOrder": 42
					},
					{
						"rate": 0.888,
						"actorId": 200114211,
						"bgColor": "hsla(69.56, 54.10%, 49.03%, 1.00)",
						"actorHId": "6ed16778-8b1c-144b-5bb4-0e235bdc5cf1",
						"actorINN": [
							"7735578947"
						],
						"actorKey": "15221413",
						"actorCats": null,
						"actorName": "ООО \"МИЛАНДР ЭК\"",
						"actorOGRN": "1117746552494",
						"rateOrder": 43
					},
					{
						"rate": 0.887,
						"actorId": 200096057,
						"bgColor": "hsla(69.47, 54.16%, 49.12%, 1.00)",
						"actorHId": "6514150b-e193-7aac-7ce3-5df970e6e460",
						"actorINN": [
							"7728793929"
						],
						"actorKey": "15263620",
						"actorCats": null,
						"actorName": "ООО \"ГСЭК\"",
						"actorOGRN": "5117746035413",
						"rateOrder": 44
					},
					{
						"rate": 0.887,
						"actorId": 200014602,
						"bgColor": "hsla(69.44, 54.18%, 49.14%, 1.00)",
						"actorHId": "f12f7aa7-4370-fb90-4669-2e41d2b12fd6",
						"actorINN": [
							"6732139675"
						],
						"actorKey": "20526153",
						"actorCats": null,
						"actorName": "АО \"ИНТЕГРАЛ-ЗАПАД\"",
						"actorOGRN": "1176733001840",
						"rateOrder": 45
					},
					{
						"rate": 0.886,
						"actorId": 200074048,
						"bgColor": "hsla(69.37, 54.23%, 49.21%, 1.00)",
						"actorHId": "0293e529-a09f-887d-86bd-129fa74e7138",
						"actorINN": [
							"3625014281"
						],
						"actorKey": "18025250",
						"actorCats": null,
						"actorName": "ООО\"КУВЕРА-ТЭК\"",
						"actorOGRN": "1163668087527",
						"rateOrder": 46
					},
					{
						"rate": 0.886,
						"actorId": 200112358,
						"bgColor": "hsla(69.37, 54.23%, 49.21%, 1.00)",
						"actorHId": "72b65776-62b3-48f4-fa54-bac7888d332d",
						"actorINN": [
							"5405006867"
						],
						"actorKey": "21573909",
						"actorCats": null,
						"actorName": "ООО \"НТЕХ\"",
						"actorOGRN": "1175476086719",
						"rateOrder": 47
					},
					{
						"rate": 0.886,
						"actorId": 200150654,
						"bgColor": "hsla(69.37, 54.23%, 49.21%, 1.00)",
						"actorHId": "94d8864b-4dbb-6bcb-6c60-8b0d93dfdcf0",
						"actorINN": [
							"7719058323"
						],
						"actorKey": "15210128",
						"actorCats": null,
						"actorName": "ООО \"ФАВОРИТ-ЭК\"",
						"actorOGRN": "1147748134148",
						"rateOrder": 48
					},
					{
						"rate": 0.885,
						"actorId": 200087603,
						"bgColor": "hsla(69.24, 54.31%, 49.32%, 1.00)",
						"actorHId": "f869e856-fde1-f7fb-0020-08ac8c45df4d",
						"actorINN": [
							"7702845812"
						],
						"actorKey": "1682573",
						"actorCats": null,
						"actorName": "ООО \"ДОН\"",
						"actorOGRN": "5147746157224",
						"rateOrder": 49
					},
					{
						"rate": 0.885,
						"actorId": 200124331,
						"bgColor": "hsla(69.17, 54.36%, 49.39%, 1.00)",
						"actorHId": "83caa48e-feb8-ec01-8e02-0073fd85cfd4",
						"actorINN": [
							"6453045593"
						],
						"actorKey": "20606478",
						"actorCats": null,
						"actorName": "АО \"СЕМАЛ\"",
						"actorOGRN": "1026403050750",
						"rateOrder": 50
					},
					{
						"rate": 0.885,
						"actorId": 200094618,
						"bgColor": "hsla(69.17, 54.36%, 49.39%, 1.00)",
						"actorHId": "59133b4f-9b0c-10d9-a740-0e66c905a75c",
						"actorINN": [
							"9717063811"
						],
						"actorKey": "15913109",
						"actorCats": null,
						"actorName": "ООО \"ГТС ГРУПП\"",
						"actorOGRN": "1177746940260",
						"rateOrder": 51
					},
					{
						"rate": 0.885,
						"actorId": 200089324,
						"bgColor": "hsla(69.17, 54.36%, 49.39%, 1.00)",
						"actorHId": "4be48c0b-2d4e-27d2-d0c4-2ca438a17ce5",
						"actorINN": [
							"7841484779"
						],
						"actorKey": "19841271",
						"actorCats": null,
						"actorName": "ООО \"ДИЭЛКОМ\"",
						"actorOGRN": "1137847235350",
						"rateOrder": 52
					},
					{
						"rate": 0.885,
						"actorId": 200174098,
						"bgColor": "hsla(69.17, 54.36%, 49.39%, 1.00)",
						"actorHId": "920dc1b4-7738-d5ef-2431-e37741e5db77",
						"actorINN": [
							"5406575513"
						],
						"actorKey": "18958045",
						"actorCats": null,
						"actorName": "ООО \"КОМПАНИЯ ВЕКТОР\"",
						"actorOGRN": "1155476000536",
						"rateOrder": 53
					},
					{
						"rate": 0.885,
						"actorId": 200009039,
						"bgColor": "hsla(69.17, 54.36%, 49.39%, 1.00)",
						"actorHId": "6c65cd14-9230-17f8-7649-c698b0236c0a",
						"actorINN": [
							"7727657240"
						],
						"actorKey": "1798721",
						"actorCats": null,
						"actorName": "ООО \"НАУТЕХ\"",
						"actorOGRN": "1087746849970",
						"rateOrder": 54
					},
					{
						"rate": 0.884,
						"actorId": 200164438,
						"bgColor": "hsla(69.13, 54.39%, 49.43%, 1.00)",
						"actorHId": "41d1ed2a-fe39-8f62-16c4-604c84d7dcc7",
						"actorINN": [
							"7709987950"
						],
						"actorKey": "15871385",
						"actorCats": null,
						"actorName": "ООО \"ОНЭЛЕК ВПК\"",
						"actorOGRN": "1177746112125",
						"rateOrder": 55
					},
					{
						"rate": 0.884,
						"actorId": 200030610,
						"bgColor": "hsla(69.09, 54.42%, 49.47%, 1.00)",
						"actorHId": "675c4551-d503-66be-003a-e321082b0b89",
						"actorINN": [
							"7733805224"
						],
						"actorKey": "15550349",
						"actorCats": null,
						"actorName": "ООО \"АТРИЛОР\"",
						"actorOGRN": "1127746452635",
						"rateOrder": 56
					},
					{
						"rate": 0.883,
						"actorId": 200099606,
						"bgColor": "hsla(69.01, 54.46%, 49.53%, 1.00)",
						"actorHId": "19cd28cb-e385-8a3b-59da-788c39ec3ede",
						"actorINN": [
							"7713122621"
						],
						"actorKey": "15479687",
						"actorCats": null,
						"actorName": "ЗАО \"РТКТ\"",
						"actorOGRN": "1027739267126",
						"rateOrder": 57
					},
					{
						"rate": 0.883,
						"actorId": 200060677,
						"bgColor": "hsla(68.99, 54.48%, 49.55%, 1.00)",
						"actorHId": "de99d539-5f47-6e7b-1652-2885ff9cbc93",
						"actorINN": [
							"7728792756"
						],
						"actorKey": "1634073",
						"actorCats": null,
						"actorName": "АО \"РАДИАНТ-ЭК\"",
						"actorOGRN": "1117746996377",
						"rateOrder": 58
					},
					{
						"rate": 0.883,
						"actorId": 200169368,
						"bgColor": "hsla(68.96, 54.50%, 49.58%, 1.00)",
						"actorHId": "ec8a0de6-cfe0-2649-aa19-f5bcc44a712e",
						"actorINN": [
							"7612024201"
						],
						"actorKey": "15521338",
						"actorCats": null,
						"actorName": "АО \"РОССПЕЦПОСТАВКА\"",
						"actorOGRN": "1027601301639",
						"rateOrder": 59
					},
					{
						"rate": 0.882,
						"actorId": 200003766,
						"bgColor": "hsla(68.90, 54.54%, 49.64%, 1.00)",
						"actorHId": "1c8b958b-2193-a8e9-168b-099b6416a72d",
						"actorINN": [
							"5407216683"
						],
						"actorKey": "19075053",
						"actorCats": null,
						"actorName": "ООО \"МИКРОСАН\"",
						"actorOGRN": "1025403209182",
						"rateOrder": 60
					},
					{
						"rate": 0.882,
						"actorId": 200112687,
						"bgColor": "hsla(68.83, 54.58%, 49.70%, 1.00)",
						"actorHId": "7c0ad841-6c3b-2c69-92fc-cc4855f583fc",
						"actorINN": [
							"7813336306"
						],
						"actorKey": "19666388",
						"actorCats": null,
						"actorName": "ГАММА ПЛЮС ООО",
						"actorOGRN": "1057813173835",
						"rateOrder": 61
					},
					{
						"rate": 0.881,
						"actorId": 200018810,
						"bgColor": "hsla(68.82, 54.59%, 49.71%, 1.00)",
						"actorHId": "70f8dd9a-fa8c-b1fd-6310-3478b3523ed9",
						"actorINN": [
							"7841490081"
						],
						"actorKey": "19906373",
						"actorCats": null,
						"actorName": "ООО \"ГК \"СНАБЖЕНИЕ\"",
						"actorOGRN": "1137847395410",
						"rateOrder": 62
					},
					{
						"rate": 0.881,
						"actorId": 200092116,
						"bgColor": "hsla(68.81, 54.60%, 49.72%, 1.00)",
						"actorHId": "e45b5eb0-c4e5-7972-7670-cacf2bf5b58e",
						"actorINN": [
							"8602105968"
						],
						"actorKey": "15381831",
						"actorCats": [
							5
						],
						"actorName": "ООО \"СИБИРСКИЙ ПРОЕКТ\"",
						"actorOGRN": "1028600580491",
						"rateOrder": 63
					},
					{
						"rate": 0.881,
						"actorId": 200129954,
						"bgColor": "hsla(68.81, 54.60%, 49.72%, 1.00)",
						"actorHId": "e0b3d2ce-72b2-0eba-3633-16f1c516dde3",
						"actorINN": [
							"7813389280"
						],
						"actorKey": "19918593",
						"actorCats": null,
						"actorName": "АО \"НЕВА ЭЛЕКТРОНИКА\"",
						"actorOGRN": "1077847571669",
						"rateOrder": 64
					},
					{
						"rate": 0.881,
						"actorId": 200153007,
						"bgColor": "hsla(68.81, 54.60%, 49.72%, 1.00)",
						"actorHId": "a9f11044-1524-c312-851a-3f4fb2b8225e",
						"actorINN": [
							"7726371311"
						],
						"actorKey": "15404577",
						"actorCats": null,
						"actorName": "ООО \"КОМПАНИЯ \"НТНК\"",
						"actorOGRN": "1167746255600",
						"rateOrder": 65
					},
					{
						"rate": 0.881,
						"actorId": 200111959,
						"bgColor": "hsla(68.74, 54.64%, 49.78%, 1.00)",
						"actorHId": "448bf7fd-6370-7734-1cc9-cf5c1fc9939a",
						"actorINN": [
							"2460218881"
						],
						"actorKey": "18256106",
						"actorCats": null,
						"actorName": "ООО \"АТМ\"",
						"actorOGRN": "1092468044646",
						"rateOrder": 66
					},
					{
						"rate": 0.881,
						"actorId": 200154167,
						"bgColor": "hsla(68.74, 54.64%, 49.78%, 1.00)",
						"actorHId": "f1709135-8365-fe65-c317-ad2f19bb7518",
						"actorINN": [
							"2452038927"
						],
						"actorKey": "18239140",
						"actorCats": null,
						"actorName": "ООО \"РЕКОН\"",
						"actorOGRN": "1112452000330",
						"rateOrder": 67
					},
					{
						"rate": 0.881,
						"actorId": 200126154,
						"bgColor": "hsla(68.74, 54.64%, 49.78%, 1.00)",
						"actorHId": "c65c19c8-4ace-743b-e239-734fb48cd4b6",
						"actorINN": [
							"7722858055"
						],
						"actorKey": "15591376",
						"actorCats": null,
						"actorName": "ООО \"ТЕСТЭЛЕКТРО\"",
						"actorOGRN": "5147746201378",
						"rateOrder": 68
					},
					{
						"rate": 0.881,
						"actorId": 200039764,
						"bgColor": "hsla(68.74, 54.64%, 49.78%, 1.00)",
						"actorHId": "3429d13f-b0ec-1397-67c5-6120616b99ac",
						"actorINN": [
							"7807365362"
						],
						"actorKey": "19806508",
						"actorCats": null,
						"actorName": "ООО \"ТЕХНОЛОГИЯ С-ПБ\"",
						"actorOGRN": "1117847531713",
						"rateOrder": 69
					},
					{
						"rate": 0.881,
						"actorId": 200018969,
						"bgColor": "hsla(68.74, 54.64%, 49.78%, 1.00)",
						"actorHId": "8d6a98a8-2610-cb7b-9e99-baf7a07e114e",
						"actorINN": [
							"7715256236"
						],
						"actorKey": "15533015",
						"actorCats": null,
						"actorName": "ООО \"ЭЛЕКТРОТЕХНИКА КОМПОНЕНТ\"",
						"actorOGRN": "1027700488133",
						"rateOrder": 70
					},
					{
						"rate": 0.88,
						"actorId": 200087548,
						"bgColor": "hsla(68.69, 54.68%, 49.83%, 1.00)",
						"actorHId": "24b598c9-e5ca-1703-2115-accd2f04d883",
						"actorINN": [
							"7733618993"
						],
						"actorKey": "15167997",
						"actorCats": null,
						"actorName": "АО \"СПЕЦ-ЭК\"",
						"actorOGRN": "1077759103542",
						"rateOrder": 71
					},
					{
						"rate": 0.88,
						"actorId": 200164259,
						"bgColor": "hsla(68.63, 54.72%, 49.88%, 1.00)",
						"actorHId": "42e7101f-00da-6dd9-a104-bf786b6bc107",
						"actorINN": [
							"4632202922"
						],
						"actorKey": "20293388",
						"actorCats": null,
						"actorName": "ООО \"ИНТЕР ЧИП КОМПЛЕКТ\"",
						"actorOGRN": "1154632004251",
						"rateOrder": 72
					},
					{
						"rate": 0.88,
						"actorId": 200088191,
						"bgColor": "hsla(68.63, 54.72%, 49.88%, 1.00)",
						"actorHId": "56c8b4fb-4538-e603-ac8d-3ee3f0aa7137",
						"actorINN": [
							"4025055496"
						],
						"actorKey": "20471876",
						"actorCats": null,
						"actorName": "ООО \"ТЕХНО-ПЛЮС\"",
						"actorOGRN": "1024000952414",
						"rateOrder": 73
					},
					{
						"rate": 0.88,
						"actorId": 200138921,
						"bgColor": "hsla(68.63, 54.72%, 49.88%, 1.00)",
						"actorHId": "79479331-1583-7d95-f7aa-17a0da9618c7",
						"actorINN": [
							"7841486984"
						],
						"actorKey": "19838754",
						"actorCats": null,
						"actorName": "ООО \"ЧИПКОНТАКТ\"",
						"actorOGRN": "1137847317057",
						"rateOrder": 74
					},
					{
						"rate": 0.88,
						"actorId": 200092135,
						"bgColor": "hsla(68.61, 54.73%, 49.90%, 1.00)",
						"actorHId": "406f5623-d1d8-1049-cdba-c08590a4025b",
						"actorINN": [
							"1831050860"
						],
						"actorKey": "20998049",
						"actorCats": [
							5
						],
						"actorName": "АО \"НПФ \"РАДИО-СЕРВИС\"",
						"actorOGRN": "1021801146872",
						"rateOrder": 75
					},
					{
						"rate": 0.879,
						"actorId": 200012455,
						"bgColor": "hsla(68.57, 54.75%, 49.94%, 1.00)",
						"actorHId": "c8d509c8-d72e-69b9-23fd-ba48b821c6d3",
						"actorINN": [
							"7724179933"
						],
						"actorKey": "1629141",
						"actorCats": [
							4
						],
						"actorName": "АО \"ЭНПО СПЭЛС\"",
						"actorOGRN": "1027739504561",
						"rateOrder": 76
					},
					{
						"rate": 0.879,
						"actorId": 200128763,
						"bgColor": "hsla(68.54, 54.77%, 49.96%, 1.00)",
						"actorHId": "a355894a-eb25-f35e-7897-e45618a2f22c",
						"actorINN": [
							"6731063247"
						],
						"actorKey": "20526103",
						"actorCats": null,
						"actorName": "АО \"ЭКСИТОН\"",
						"actorOGRN": "1076731012346",
						"rateOrder": 77
					},
					{
						"rate": 0.878,
						"actorId": 200023186,
						"bgColor": "hsla(68.46, 54.83%, 50.04%, 1.00)",
						"actorHId": "6277e591-35be-4859-f265-232854da00c0",
						"actorINN": [
							"7816244653"
						],
						"actorKey": "19615370",
						"actorCats": null,
						"actorName": "ООО \"Т-КОМПОНЕНТ\"",
						"actorOGRN": "1157847044849",
						"rateOrder": 78
					},
					{
						"rate": 0.878,
						"actorId": 200105050,
						"bgColor": "hsla(68.45, 54.83%, 50.05%, 1.00)",
						"actorHId": "7bdc8c19-879e-f2af-c6d8-2d4666bdb02a",
						"actorINN": [
							"7743831248"
						],
						"actorKey": "15240368",
						"actorCats": null,
						"actorName": "АО \"АНТАРЕС\"",
						"actorOGRN": "1117746785936",
						"rateOrder": 79
					},
					{
						"rate": 0.877,
						"actorId": 200098659,
						"bgColor": "hsla(68.36, 54.89%, 50.12%, 1.00)",
						"actorHId": "4e5dd996-6507-b927-e4c8-fce0f4a03b67",
						"actorINN": [
							"1831045651"
						],
						"actorKey": "20998047",
						"actorCats": null,
						"actorName": "ООО\"МДС\"",
						"actorOGRN": "1021801160732",
						"rateOrder": 80
					},
					{
						"rate": 0.877,
						"actorId": 200004360,
						"bgColor": "hsla(68.36, 54.89%, 50.12%, 1.00)",
						"actorHId": "4068b1a2-d794-851b-e555-eb3b3e2c639a",
						"actorINN": [
							"5043057314"
						],
						"actorKey": "19494073",
						"actorCats": null,
						"actorName": "ООО \"НОМИНАЛ\"",
						"actorOGRN": "1165043050347",
						"rateOrder": 81
					},
					{
						"rate": 0.877,
						"actorId": 200128580,
						"bgColor": "hsla(68.35, 54.90%, 50.14%, 1.00)",
						"actorHId": "7f702066-58d7-a3f4-61bb-0456ea0e8d92",
						"actorINN": [
							"7802729217"
						],
						"actorKey": "19777759",
						"actorCats": null,
						"actorName": "ООО \"КВЕСТ\"",
						"actorOGRN": "1107847358222",
						"rateOrder": 82
					},
					{
						"rate": 0.877,
						"actorId": 200115499,
						"bgColor": "hsla(68.34, 54.90%, 50.14%, 1.00)",
						"actorHId": "110ad8e1-7c9d-f056-8f2a-887dd28ed087",
						"actorINN": [
							"7806296652"
						],
						"actorKey": "21916492",
						"actorCats": null,
						"actorName": "ООО \"СИММЕТРОН ЭК\"",
						"actorOGRN": "1187847001341",
						"rateOrder": 83
					},
					{
						"rate": 0.877,
						"actorId": 200116956,
						"bgColor": "hsla(68.32, 54.92%, 50.17%, 1.00)",
						"actorHId": "5691e750-cd00-b145-57ce-3736de55d253",
						"actorINN": [
							"7804542790"
						],
						"actorKey": "19800575",
						"actorCats": null,
						"actorName": "ООО \"НТЦ ЭЛЕКОМ СПБ\"",
						"actorOGRN": "1147847357954",
						"rateOrder": 84
					},
					{
						"rate": 0.877,
						"actorId": 200135194,
						"bgColor": "hsla(68.31, 54.92%, 50.17%, 1.00)",
						"actorHId": "b3899144-27ae-3538-1e97-3238413f2451",
						"actorINN": [
							"7734111035"
						],
						"actorKey": "1629075",
						"actorCats": [
							5,
							4
						],
						"actorName": "НИЦ \"КУРЧАТОВСКИЙ ИНСТИТУТ\"",
						"actorOGRN": "1027739576006",
						"rateOrder": 85
					},
					{
						"rate": 0.877,
						"actorId": 200056389,
						"bgColor": "hsla(68.31, 54.92%, 50.17%, 1.00)",
						"actorHId": "01ec1142-7fb4-75fc-1e13-0ec19e28ee2e",
						"actorINN": [
							"2462231990"
						],
						"actorKey": "18280501",
						"actorCats": null,
						"actorName": "ООО КБ \"ПРИКЛАДНАЯ АВТОМАТИКА\"",
						"actorOGRN": "1142468039922",
						"rateOrder": 86
					},
					{
						"rate": 0.877,
						"actorId": 200092065,
						"bgColor": "hsla(68.31, 54.92%, 50.17%, 1.00)",
						"actorHId": "2bab21fc-a1f7-cd69-a5f1-8f4133d9105b",
						"actorINN": [
							"7448190861"
						],
						"actorKey": "18639575",
						"actorCats": null,
						"actorName": "ООО НПК \"СИМ\"",
						"actorOGRN": "1167456070924",
						"rateOrder": 87
					},
					{
						"rate": 0.877,
						"actorId": 200130289,
						"bgColor": "hsla(68.27, 54.95%, 50.21%, 1.00)",
						"actorHId": "5d1f8e23-f69b-050b-ac6c-6b8ed86ea4fa",
						"actorINN": [
							"7710046387"
						],
						"actorKey": "1636548",
						"actorCats": null,
						"actorName": "ООО \"КОНТАКТ ЭЛЕКТРОН\"",
						"actorOGRN": "1037739770375",
						"rateOrder": 88
					},
					{
						"rate": 0.877,
						"actorId": 200063746,
						"bgColor": "hsla(68.27, 54.95%, 50.21%, 1.00)",
						"actorHId": "b34d617f-7263-2c51-c644-3273bc95e5ef",
						"actorINN": [
							"7810895610"
						],
						"actorKey": "19827887",
						"actorCats": null,
						"actorName": "ООО \"МАКРО ЕМС\"",
						"actorOGRN": "1129847011150",
						"rateOrder": 89
					},
					{
						"rate": 0.877,
						"actorId": 200041306,
						"bgColor": "hsla(68.27, 54.95%, 50.21%, 1.00)",
						"actorHId": "2efe9c63-40df-29af-8660-e190ff847fd7",
						"actorINN": [
							"7723329953"
						],
						"actorKey": "15516839",
						"actorCats": null,
						"actorName": "ООО \"ЭЛНИКА\"",
						"actorOGRN": "1037723008840",
						"rateOrder": 90
					},
					{
						"rate": 0.876,
						"actorId": 200051758,
						"bgColor": "hsla(68.25, 54.96%, 50.23%, 1.00)",
						"actorHId": "d5af7be9-a92a-0230-93fd-b3764bc6b824",
						"actorINN": [
							"7721742065"
						],
						"actorKey": "15268426",
						"actorCats": [
							5
						],
						"actorName": "ООО \"ТЕСТ-КОНТАКТ\"",
						"actorOGRN": "1117746937461",
						"rateOrder": 91
					},
					{
						"rate": 0.876,
						"actorId": 200018265,
						"bgColor": "hsla(68.24, 54.97%, 50.24%, 1.00)",
						"actorHId": "081e62cf-5566-b130-85b1-bf572572606b",
						"actorINN": [
							"7720134018"
						],
						"actorKey": "15478759",
						"actorCats": null,
						"actorName": "ООО \"МАКРО ТИМ\"",
						"actorOGRN": "1027739020759",
						"rateOrder": 92
					},
					{
						"rate": 0.3,
						"actorId": 200157793,
						"bgColor": "hsla(68.19, 55.00%, 50.28%, 1.00)",
						"actorHId": "11e980a2-f0a8-1463-36cb-6be17d135806",
						"actorINN": [
							"6671434007"
						],
						"actorKey": "19360253",
						"actorCats": null,
						"actorName": "ООО \"ПЕНТАКОМ\"",
						"actorOGRN": "1136671033674",
						"rateOrder": 93
					},
					{
						"rate": 0.873,
						"actorId": 200022865,
						"bgColor": "hsla(67.92, 55.18%, 50.53%, 1.00)",
						"actorHId": "f66e13c9-9d27-f897-d812-34ae6376a93b",
						"actorINN": [
							"7735010706"
						],
						"actorKey": "1628732",
						"actorCats": [
							5
						],
						"actorName": "АО \"АНГСТРЕМ\"",
						"actorOGRN": "1027700140930",
						"rateOrder": 94
					},
					{
						"rate": 0.873,
						"actorId": 200083964,
						"bgColor": "hsla(67.92, 55.18%, 50.53%, 1.00)",
						"actorHId": "fc66446f-d063-97f6-cdc1-6f0d425cb5a8",
						"actorINN": [
							"7709163294"
						],
						"actorKey": "15494460",
						"actorCats": null,
						"actorName": "АО \"НПО \"ТРАНСКОМ\"",
						"actorOGRN": "1027739687975",
						"rateOrder": 95
					},
					{
						"rate": 0.873,
						"actorId": 200129377,
						"bgColor": "hsla(67.90, 55.19%, 50.54%, 1.00)",
						"actorHId": "9fdbd05d-8b27-f715-eed2-62698513a956",
						"actorINN": [
							"7735040690"
						],
						"actorKey": "1629822",
						"actorCats": [
							4,
							5
						],
						"actorName": "АО \"ПКК МИЛАНДР\"",
						"actorOGRN": "1027739083921",
						"rateOrder": 96
					},
					{
						"rate": 0.873,
						"actorId": 200044308,
						"bgColor": "hsla(67.90, 55.20%, 50.55%, 1.00)",
						"actorHId": "26c65959-a696-3fdf-d4f9-6f32e5e7b044",
						"actorINN": [
							"6671267564"
						],
						"actorKey": "19348553",
						"actorCats": null,
						"actorName": "ООО \"АБТРОНИКС\"",
						"actorOGRN": "1086671013043",
						"rateOrder": 97
					},
					{
						"rate": 0.873,
						"actorId": 200108797,
						"bgColor": "hsla(67.90, 55.20%, 50.55%, 1.00)",
						"actorHId": "3d8fa938-911b-d9c1-24ea-02d9d8f07c39",
						"actorINN": [
							"5406259290"
						],
						"actorKey": "19062122",
						"actorCats": [
							5
						],
						"actorName": "ООО \"АЙ-ТИ-СИ\" К°\"",
						"actorOGRN": "1035402500825",
						"rateOrder": 98
					},
					{
						"rate": 0.873,
						"actorId": 200085453,
						"bgColor": "hsla(67.90, 55.20%, 50.55%, 1.00)",
						"actorHId": "2b91590c-d5c0-c672-5981-e761006e5a73",
						"actorINN": [
							"7806078196"
						],
						"actorKey": "19789545",
						"actorCats": null,
						"actorName": "ООО \"ТЕХНОСТЕК-ДИСТРИБУЦИЯ\"",
						"actorOGRN": "1157847002774",
						"rateOrder": 99
					},
					{
						"rate": 0.873,
						"actorId": 200160207,
						"bgColor": "hsla(67.90, 55.20%, 50.55%, 1.00)",
						"actorHId": "e30079ab-6ec2-2275-7345-86700b93b6bc",
						"actorINN": [
							"6453011530"
						],
						"actorKey": "21298720",
						"actorCats": null,
						"actorName": "ПАО \"ТАНТАЛ\"",
						"actorOGRN": "1026403042038",
						"rateOrder": 100
					}
				],
				"actorsNumber": 100
			},
			"queryString": "микросхемы",
			"executionTime": "00:00:00.170037",
			"actorCategories": [
				"",
				"ИТ",
				"Инжиниринг",
				"Инфраструктура поддержки",
				"Наука",
				"Производство"
			]
		}
	return JSON.stringify(companies);
}

$(document).ready(function () {
	var responseOKPD2 = serverRequest.getOKPD2();
	var okpd2 = JSON.parse(responseOKPD2);
	for(var i = 0; i < okpd2.OKPD2.length; i++)
	{
		$('.chosen-select').append('<option>' + okpd2.OKPD2[i].OKPD2Text + ' (' + okpd2.OKPD2[i].OKPD2Code + ')' + '</option>');
	}
	$(".chosen-select").chosen({
		no_results_text: "Пусто для "
	});
	$(".chosen-select-company").chosen();
	$('.chosen-choices').css('padding', '8px');

	$('.chosen-select').change(function() {
		var valOkpd2 = $(this).find('option:selected').val();
		if(valOkpd2 !== undefined)
		{
			var responseCompanies = serverRequest.getCompanies();
			var companies = JSON.parse(responseCompanies);
			var actors = companies.agsActors.actors;
			for(var i = 0; i < actors.length; i++)
			{
				var actor = actors[i];
				var name = actor.actorName + ' ( ИНН: ' + actor.actorINN[0] + ', ОГРН: ' + actor.actorOGRN + ')';
				var bgColor = actor.bgColor.replace(/(1\.00)/,actor.rate);
				$('.chosen-select-company').append('<option data-name=\'' + actor.actorName + '\' data-inn="' + actor.actorINN[0] + '" data-ogrn="' + actor.actorOGRN + '" style="background-color: ' + bgColor + '">' + name + '</option>');
			}
			$(".chosen-select-company").trigger('chosen:updated');
			$(".chosen-select-company").parent().parent().removeClass('d-none');
		}
		else
		{
			$(".chosen-select-company").parent().parent().addClass('d-none');
			if(!$(".alert").hasClass('d-none'))
			{
				$(".alert").addClass('d-none');
			}
		}
	});

	$('.chosen-select-company').change(function(){
		if($(this).find('option:selected').val() != '')
		{
			$(".alert").css('backgroundColor',$(this).find('option:selected').css('backgroundColor'));
			$('#companyName').text($(this).find('option:selected').data('name'));
			$('#companyInn').text($(this).find('option:selected').data('inn'));
			$('#companyOgrn').text($(this).find('option:selected').data('ogrn'));
			$(".alert").removeClass('d-none');
		}
		else
		{
			$(".alert").css('backgroundColor','FFF');
			$('#companyName').empty();
			$('#companyInn').empty();
			$('#companyOgrn').empty();
			if(!$(".alert").hasClass('d-none'))
			{
				$(".alert").addClass('d-none');
			}
		}
	});
});